const PORT = 9695;//this is port number for nodejs server

//including library
var http = require('http')
var socketio = require('socket.io')
var SerialPort = require('serialport');
//end of including library

var portName = process.argv[2];//get parameter from command line
var phone = '+841682174652';

//creating connection to serial port
var serialPort = new SerialPort(portName, {
    baudrate: 9600,
    parser: SerialPort.parsers.readline('\r')
});

serialPort.on('open', function () {//when connection created
    console.log(portName + ' is ready!');
});

var ip = require('ip');
var app = http.createServer();//create application
var io = socketio(app);//create socketio
app.listen(PORT);//listen on port
console.log("Server nodejs chay tai dia chi: " + ip.address() + ":" + PORT);


var LEDController = null, alertController = null;

//when Socket Client connects to Socket Server
io.on('connection', function (socket) {

    var name;

    socket.emit('greetings', {greeting: 'Hello! Who are you'});//greeting step

    socket.on('clientGreeting', function (data) {
        // console.log(data);
        name = data.clientName;
        console.log(name + " connected");

        //handle for each connection
        if (name === 'LEDController') {
            if (LEDController !== null) {
                io.sockets.sockets[LEDController].disconnect();
            }

            LEDController = socket.id;
            socket.on('resLEDStatus', function (data) {
                socket.broadcast.emit('LEDStatus', data);
            });

            socket.emit('reqLEDStatus', '');
        } else if (name === 'AlertController') {
            if (alertController !== null) {
                io.sockets.sockets[alertController].disconnect();
            }

            alertController = socket.id;

            socket.on('alert', function (data) {
                socket.broadcast.emit('alert', 'Warning! Warning! Warning!');
                sendAlertSms();
            });

            socket.on('resAlertStatus', function (data) {
                var message = ((data.status) ? 'Warning! Warning! Warning!' : '');
                socket.broadcast.emit('alert', message);
            });
        } else {
            socket.on('reqLEDStatus', function (data) {
                reqLEDStatus(socket, data);
            });

            socket.on('reqLEDBrn', function (data) {
                reqLEDBrn(socket, data);
            });

            socket.on('offWarning', function (data) {
                socket.broadcast.emit('offWarning', data);
            });

            socket.on('autoLED', function (data) {
                autoLED(socket, data);
            });

            socket.on('setLowest', function (data) {
                setLowest(socket, data);
            });

            if (checkESPConnected(socket)) {
                io.sockets.sockets[LEDController].emit('reqLEDStatus', '');
            }

            if (alertController != null) {
                io.sockets.sockets[alertController].emit('reqAlertStatus', '');
            }
        }
    });

    // When socket client is disconnected
    socket.on('disconnect', function () {
        console.log("disconnect " + name);
        if (name === 'LEDController') {
            LEDController = null;
            socket.broadcast.emit('dcnLC', '');
        } else if (name === 'AlertController') {
            alertController = null;
        }
    });
});

function checkESPConnected(socket) {//check whether LEDController is connected
    if (LEDController === null) {
        socket.emit('dcnLC', '');
        return false;
    }

    return true;
}

function reqLEDStatus(socket, data) {
    if (!checkESPConnected(socket)) return;

    var espSocket = io.sockets.sockets[LEDController];

    espSocket.emit('reqLEDStatus', "");
}

function reqLEDBrn(socket, data) {
    if (!checkESPConnected(socket)) return;

    var espSocket = io.sockets.sockets[LEDController];
    espSocket.emit('reqLEDBrn', data);
}

function autoLED(socket, data) {
    if (!checkESPConnected(socket)) return;

    var espSocket = io.sockets.sockets[LEDController];
    var command = ((data.autoLED) ? 'enableAuto' : 'disableAuto');

    espSocket.emit(command, '');
}

function setLowest(socket, data) {
    if (!checkESPConnected(socket)) return;

    var espSocket = io.sockets.sockets[LEDController];
    espSocket.emit('setLowest', data);
}

function sendAlertSms() {
    serialPort.write("AT+CMGF=1");
    serialPort.write('\r');
    serialPort.write("AT+CMGS=\"");
    serialPort.write(phone);
    serialPort.write('"')
    serialPort.write('\r');
    serialPort.write('Theft! Theft! Theft!');
    serialPort.write(Buffer([0x1A]));
    serialPort.write('^z');
    console.log('sent')
}